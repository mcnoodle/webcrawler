﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    public class WebCrawler
    {
        //WebBrowser _webBrowser;
        HttpClient _client;

        public WebCrawler(string username, string password, string url)
        {
            InitiateClient(url);
            Login(username, password);
        }
        public void InitiateClient(String url)
        {
            
            var cookieContainer = new CookieContainer();
            var baseAddress = new Uri(url);
            _client = new HttpClient(new HttpClientHandler() { CookieContainer = cookieContainer });
            _client.BaseAddress = baseAddress;
            
            cookieContainer.Add(baseAddress, new Cookie("_ga", "GA1.2.1154068979.1578978589"));
            cookieContainer.Add(baseAddress, new Cookie("PHPSESSID", "8036r2jr0sb8bmt40ia5e0m626"));
            cookieContainer.Add(baseAddress, new Cookie("_gid", "GA1.2.1819792560.1579325797"));
            //_client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            _client.DefaultRequestHeaders.Add("Accept-Language", "en-GB,en-US;q=0.9,en;q=0.8");
            _client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
;
            //_client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");

            _client.DefaultRequestHeaders.Add("Connection", "keep-alive");
            _client.DefaultRequestHeaders.Add("Origin", "https://www.spellpundit.com");
            _client.DefaultRequestHeaders.Add("Referer", "https://www.spellpundit.com/spell/index.php");

        }
        public bool Login(string username, string password)
        {
            var cookieContainer = new CookieContainer();
            //usually i make a standard request without authentication, eg: to the home page.
                //by doing this request you store some initial cookie values, that might be used in the subsequent login request and checked by the server
                var homePageResult = _client.GetAsync("/").Result;
                  homePageResult.EnsureSuccessStatusCode();
            string plain_string_content = "user=dchinglee@hotmail.com&passwd=clj791104&terms=on";
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/index.php?mode=login")
            {
                Content = new StringContent(plain_string_content)
            };
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            var postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("user", username));
            postData.Add(new KeyValuePair<string, string>("passwd", password));
            postData.Add(new KeyValuePair<string, string>("returnUrl", "/login/")); //To simulate the browser
            postData.Add(new KeyValuePair<string, string>("service", "login")); //To simulate the browser

            HttpContent stringContent = new FormUrlEncodedContent(postData);
            var loginResult = _client.PostAsync("/index.php?mode=login", stringContent).Result;//_client.SendAsync(request).Result;
                loginResult.EnsureSuccessStatusCode();
            var c = loginResult.Content.ReadAsStringAsync();
                var result = WordSearch("imminent").Result;
                var s = result.Content.ReadAsStringAsync().Result;
                System.Diagnostics.Debug.WriteLine(s);
                //make the subsequent web requests using the same HttpClient object

            
            return true;
        }
        async Task<HttpResponseMessage> WordSearch(string word)
        {
            string plain_string_content = "flag=i&word_search=" + word;
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "?mode=word_search")
            {
                Content = new StringContent(plain_string_content)
            };
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            return await  _client.SendAsync(request);
        }
    }
}
