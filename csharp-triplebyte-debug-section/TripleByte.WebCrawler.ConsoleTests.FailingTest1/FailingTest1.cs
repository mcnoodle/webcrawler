﻿using System;

namespace TripleByte.WebCrawler.ConsoleTests.FailingTest1
{
    internal class FailingTest1
    {
        private static Crawler _crawler;

        private static void Main(string[] args)
        {
            _crawler = new Crawler(10);
            _crawler.Crawl("http://triplebyte.github.io/web-crawler-test-site/test1/");

            Console.WriteLine(_crawler.Graph.Get("http://triplebyte.github.io/web-crawler-test-site/test1/SVG_logo.svg").RequestType != RequestType.Head ? "Test FAIL: it should crawl SVG file with HEAD request" : "Test PASS");
            Console.ReadLine();
        }
    }
}
