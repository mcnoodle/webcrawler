﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleByte.WebCrawler.ConsoleTests.FailingTest2
{
    class FailingTest2
    {
        private static Crawler _crawler;

        private static void Main(string[] args)
        {
            _crawler = new Crawler(10);
            _crawler.Crawl("http://triplebyte.github.io/web-crawler-test-site/test2/");

            Console.WriteLine(_crawler.Graph.Get("http://triplebyte.github.io/web-crawler-test-site/test2/page2.html").PageNodeStatus != PageNodeStatus.Success ? "Test FAIL: it should correctly find a broken link" : "Test PASS");
            Console.ReadLine();
        }
    }
}
