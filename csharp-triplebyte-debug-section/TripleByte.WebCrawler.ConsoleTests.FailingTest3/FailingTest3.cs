﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleByte.WebCrawler.ConsoleTests.FailingTest3
{
    class FailingTest3
    {
        private static Crawler _crawler;

        // This tests that the crawler does not hang when crawling a weird page
        private static void Main(string[] args)
        {
            _crawler = new Crawler(10);
            _crawler.Crawl("http://triplebyte.github.io/web-crawler-test-site/test3/");

            Console.WriteLine(_crawler.Graph.Get("http://triplebyte.github.io/web-crawler-test-site/test3/").PageNodeStatus != PageNodeStatus.Success ? "Test FAIL" : "Test PASS");
            Console.WriteLine(_crawler.Graph.Get("http://blah.com:7091").PageNodeStatus == PageNodeStatus.None ? "Test FAIL" : "Test PASS");
            Console.ReadLine();
        }
    }
}
