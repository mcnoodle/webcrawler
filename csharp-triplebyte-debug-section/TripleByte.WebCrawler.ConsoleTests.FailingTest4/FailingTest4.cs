﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleByte.WebCrawler.ConsoleTests.FailingTest4
{
    class FailingTest4
    {
        private static Crawler _crawler;

        private static void Main(string[] args)
        {
            _crawler = new Crawler(10);
            _crawler.Crawl("http://triplebyte.github.io/web-crawler-test-site/test4/");

            Console.WriteLine(_crawler.Graph.Get("https://triplebyte.github.io/web-crawler-test-site/test4/page3").PageNodeStatus != PageNodeStatus.Success ? "Test FAIL: did not successfully crawl another weird page" : "Test PASS");
            Console.ReadLine();
        }
    }
}
