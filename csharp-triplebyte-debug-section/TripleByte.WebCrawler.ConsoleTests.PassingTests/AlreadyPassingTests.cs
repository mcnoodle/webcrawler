﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripleByte.WebCrawler.ConsoleTests.PassingTests
{
    class AlreadyPassingTests
    {
        private static Crawler _crawler;

        private static void Main(string[] args)
        {
            _crawler = new Crawler(10);
            _crawler.Crawl("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/");
            Console.WriteLine();
            Console.WriteLine(_crawler.Graph.Nodes.ContainsKey(new Uri("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/page2")) ? "PASS" : "FAIL");
            Console.WriteLine(_crawler.Graph.Nodes.ContainsKey(new Uri("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/page2-real")) ? "PASS" : "FAIL");
            Console.WriteLine(_crawler.Graph.Nodes.ContainsKey(new Uri("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/page2-fake")) ? "PASS" : "FAIL");
            Console.WriteLine(_crawler.Graph.Get("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/page2-real").PageNodeStatus == PageNodeStatus.Success ? "PASS" : "FAIL");
            Console.WriteLine(_crawler.Graph.Get("http://triplebyte.github.io/web-crawler-test-site/already-passing-tests/page2-fake").ResponseCode == 404 ? "PASS" : "FAIL");
            Console.ReadLine();
        }
    }
}
