﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace TripleByte.WebCrawler
{
    public class Crawler
    {
        private readonly CountdownEvent _countObject = new CountdownEvent(1);
        private readonly ConcurrentBag<string> _errors = new ConcurrentBag<string>();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly int _maxTasks;
        private readonly ConcurrentQueue<Uri> _queue = new ConcurrentQueue<Uri>();
        private Uri _domain;
        public WebsiteGraph Graph = new WebsiteGraph();

        public Crawler(int maxTasks)
        {
            _maxTasks = maxTasks;
            _logger.Info($"Starting crawling, maxTasks = {maxTasks}");

            // !!! This line has nothing to do with the debugging section. Again, next line has no bugs in it.
            // Adding support for TLS1.2. Currently by default .NET is only using SSL3 or TLS1.0, support for both of which has been dropped by GitHub.
            // To mitigate the issue, next line forces .NET to attmpt a TLS1.1 or TLS1.2 connection as well.
            // For description of a related issues see: https://github.com/google/google-api-dotnet-client/issues/911
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        }

        public WebsiteGraph Crawl(string initialUrlString)
        {
            _logger.Info($"Crawl {initialUrlString}");
            if (!initialUrlString.StartsWith("http"))
                initialUrlString = "http://" + initialUrlString;

            Uri initialIUri;

            try
            {
                initialIUri = new Uri(initialUrlString);
            }
            catch
            {
                return new WebsiteGraph();
            }

            _domain = new Uri(initialIUri.GetLeftPart(UriPartial.Authority));

            Enqueue(initialIUri);

            _countObject.Signal();
            _countObject.Wait();

            foreach (var error in _errors)
                _logger.Error($"Error: {error}");

            return Graph;
        }

        private void CrawlWithGetRequest(Uri uri)
        {
            _logger.Debug($"GET {uri}");
            var node = Graph.Nodes[uri];

            node.RequestType = RequestType.Get;
            var webRequestHandler = new WebRequestHandler {};
            var httpClient = new HttpClient(webRequestHandler);
            httpClient.Timeout = TimeSpan.FromSeconds(3);
            Task<HttpResponseMessage> response;
            try
            {
                var message = new HttpRequestMessage(new HttpMethod("GET"), uri);
                response = httpClient.SendAsync(message);
                node.ResponseCode = (int) response.Result.StatusCode;
            }
            catch (Exception e)
            {
                node.PageNodeStatus = PageNodeStatus.Failure;
                node.Error = e.ToString();
                _logger.Fatal($"{node.Error}");
                FinalizeCrawl();
                return;
            }


            node.PageNodeStatus = PageNodeStatus.Success;

            if (node.ResponseCode >= 400)
            {
                _errors.Add("When crawling " + uri + " got a " + node.ResponseCode + " (linked from " + Graph.Parents(node.Uri) + ")");
            }
            else
            {
                if (response.Result.Content.Headers.ContentType.MediaType.StartsWith("text"))
                {
                    var body = response.Result.Content.ReadAsStringAsync().Result;
                    foreach (var neighborUri in HtmlHelper.GetNeighbors(body, uri, _errors))
                    {
                        Graph.AddNeighbor(uri, neighborUri);
                        Enqueue(neighborUri);
                    }
                }
            }

            FinalizeCrawl();
        }

        private void CrawlWithHeadRequest(Uri uri)
        {
            _logger.Debug($"HEAD {uri}");
            var node = Graph.Nodes[uri];

            node.RequestType = RequestType.Head;
            var webRequestHandler = new WebRequestHandler {};
            var httpClient = new HttpClient(webRequestHandler);
            httpClient.Timeout = TimeSpan.FromSeconds(3);
            try
            {
                var message = new HttpRequestMessage(new HttpMethod("HEAD"), uri);
                var response = httpClient.SendAsync(message).Result;

                node.ResponseCode = (int) response.StatusCode;
            }
            catch (Exception e)
            {
                node.PageNodeStatus = PageNodeStatus.Failure;
                node.Error = e.ToString();
                _logger.Fatal($"{node.Error}");
                return;
            }

            node.PageNodeStatus = PageNodeStatus.Success;

            if (node.ResponseCode >= 400)
                _errors.Add($"When crawling {uri} got a {node.ResponseCode} (linked from {Graph.Parents(node.Uri)})");

            FinalizeCrawl();
        }

        private void Enqueue(Uri uri)
        {
            if (Graph.Nodes.ContainsKey(uri))
                return;

            _logger.Trace($"Enqueued {uri}");

            Graph.AddNode(uri);
            Graph.Get(uri).PageNodeStatus = PageNodeStatus.Enqueued;


            if (_countObject.CurrentCount < _maxTasks)
                SpawnCrawlingThread(uri);
            else
                _queue.Enqueue(uri);
        }

        private void FinalizeCrawl()
        {
            _logger.Trace($"Finalizing Crawl");
            try
            {
                if (!_queue.Any())
                {
                    _logger.Trace($"Queue is empty");
                    return;
                }
                Uri uriNext;
                if (_queue.TryDequeue(out uriNext))
                {
                    SpawnCrawlingThread(uriNext);
                }
            }
            finally
            {
                _logger.Trace($"Signaling awaiting threads");
                _countObject.Signal();
            }
        }

        private void SpawnCrawlingThread(Uri uri)
        {
            var threadName = $"CRAWLER-{uri.ToString().Substring(Math.Max(0, uri.ToString().Length - 8))}";
            _logger.Trace($"Spawing a thread for URL:{uri}. Thread name is {threadName}");
            _countObject.AddCount();
            var thread = new Thread(SpinUpNewCrawlingTask) {Name = threadName, IsBackground = true};
            thread.Start(uri);
        }

        private void SpinUpNewCrawlingTask(object o)
        {
            var uri = o as Uri;
            _logger.Trace($"Entering new thread for {uri}");
            if (UriShouldBeCrawledAsNode(uri))
                CrawlWithGetRequest(uri);
            else
                CrawlWithHeadRequest(uri);
        }

        private bool UriShouldBeCrawledAsNode(Uri uri)
        {
            _logger.Trace($"Deciding whether {uri} should be crawled as node or not");
            if (!_domain.Host.Equals(uri.Host))
                return false;

            // prevent you from starting to crawl FTP if you're looking at HTTP
            if (!_domain.Scheme.Equals(uri.Scheme))
                return false;

            var filetypeList = new List<string> {"pdf", "jpg", "gif", "js", "css", "png"};

            var i = uri.PathAndQuery.LastIndexOf('.');
            if (i <= 0)
                return true;
            var extension = uri.PathAndQuery.Substring(i + 1);

            return !filetypeList.Contains(extension);
        }
    }
}
