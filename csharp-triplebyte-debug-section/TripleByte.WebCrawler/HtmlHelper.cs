﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace TripleByte.WebCrawler
{
    public static class HtmlHelper
    {
        private static string CleanUpHref(string href)
        {
            return href.Replace(" ", "%20");
        }

        public static IEnumerable<Uri> GetNeighbors(string bodyStr, Uri uri, ConcurrentBag<string> errors)
        {
            var pathsToFollow = GetUrlStringsFromDoc(bodyStr);
            return GetUrlsFromPage(pathsToFollow, uri, errors);
        }

        private static IEnumerable<Uri> GetUrlsFromPage(IEnumerable<string> pathsToFollow, Uri parentUri, ConcurrentBag<string> errors)
        {
            var _out = new List<Uri>();

            foreach (var hrefRaw in pathsToFollow)
            {
                if (hrefRaw == "#" || string.IsNullOrWhiteSpace(hrefRaw))
                {
                    continue;
                }

                var href = CleanUpHref(hrefRaw);

                if (!Uri.IsWellFormedUriString(href, UriKind.RelativeOrAbsolute))
                {
                    errors.Add("There was an invalid url in " + parentUri + ": " + href);
                    continue;
                }

                Uri hrefUri;
                try
                {
                    hrefUri = new Uri(parentUri, href);
                }
                catch (Exception)
                {
                    errors.Add("There was an invalid url in " + parentUri + ": " + href);
                    continue;
                }

                if (hrefUri.Scheme == "mailto")
                    continue;

                var path = Regex.Replace(hrefUri.PathAndQuery, @"/+", @"/");
                var urlBuilder = new UriBuilder(hrefUri.Scheme, hrefUri.Host, hrefUri.Port, path);

                _out.Add(urlBuilder.Uri);
            }

            return _out;
        }

        private static IEnumerable<string> GetUrlStringsFromDoc(string bodyStr)
        {
            var matches = Regex.Matches(bodyStr, "<a [^>]*href=\"([^\"]*)");
            var _out = (from Match match in matches select match.Groups into groups select groups[1].Value).ToList();
            matches = Regex.Matches(bodyStr, "<link [^>]*href=\"([^\"]*)\"");
            _out.AddRange(from Match match in matches select match.Groups into groups select groups[1].Value);
            matches = Regex.Matches(bodyStr, "<script [^>]*src=\"([^\"]*)\"");
            _out.AddRange(from Match match in matches select match.Groups into groups select groups[1].Value);

            return _out;
        }
    }
}
