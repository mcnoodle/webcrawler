﻿using System;
using System.Collections.Generic;

namespace TripleByte.WebCrawler
{
    public class WebsiteGraph
    {
        private readonly Dictionary<Uri, List<PageNode>> _incomingLinks = new Dictionary<Uri, List<PageNode>>();
        private readonly Dictionary<Uri, List<PageNode>> _outgoingLinks = new Dictionary<Uri, List<PageNode>>();
        public Dictionary<Uri, PageNode> Nodes = new Dictionary<Uri, PageNode>();

        public void AddNeighbor(Uri fromUrl, Uri toUrl)
        {
            var arr = _outgoingLinks.ContainsKey(fromUrl) ? _outgoingLinks[fromUrl] : new List<PageNode>();
            if (Nodes.ContainsKey(toUrl))
                arr.Add(Nodes[toUrl]);


            _outgoingLinks[fromUrl] = arr;

            var arr2 = _incomingLinks.ContainsKey(fromUrl) ? _incomingLinks[fromUrl] : new List<PageNode>();
            if (Nodes.ContainsKey(fromUrl))
                arr2.Add(Nodes[fromUrl]);
            _incomingLinks[toUrl] = arr2;
        }

        public void AddNode(Uri uri)
        {
            if (!Nodes.ContainsKey(uri))
                Nodes.Add(uri, new PageNode(uri));
        }

        public PageNode Get(Uri url)
        {
            if (!Nodes.ContainsKey(url))
                AddNode(url);
            return Nodes[url];
        }

        public PageNode Get(string urlString)
        {
            return Get(new Uri(urlString));
        }

        public IList<PageNode> Parents(Uri url)
        {
            return _incomingLinks.ContainsKey(url) ? _incomingLinks[url] : new List<PageNode>();
        }
    }

    public class PageNode
    {
        public Uri Uri;

        public PageNode(Uri uri)
        {
            PageNodeStatus = PageNodeStatus.None;
            Uri = uri;
        }

        public string Error { get; set; }
        public PageNodeStatus PageNodeStatus { get; set; }

        public RequestType RequestType { get; set; }
        public int ResponseCode { get; set; }
    }

    public enum PageNodeStatus
    {
        None,
        Enqueued,
        Success,
        Failure
    }

    public enum RequestType
    {
        Get,
        Head
    }
}
